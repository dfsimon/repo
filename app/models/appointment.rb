class Appointment < ActiveRecord::Base
  belongs_to :specialist
  belongs_to :patient

  validates_presence_of :fee
  validates_numericality_of :fee, :greater_than => 0
end
