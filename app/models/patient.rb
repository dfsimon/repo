class Patient < ActiveRecord::Base
  has_many :appointment

  has_many :specialist, :through => :appointment
  belongs_to :insurance
end
