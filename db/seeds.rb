# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

insurances = Insurance.create([{ name: 'Blue Corp' }, { street_address: '2132 Radd Street' }])
insurances = Insurance.create([{ name: 'Breadwinners' }, { street_address: '1236 Trinh Lane' }])
insurances = Insurance.create([{ name: 'Core Score' }, { street_address: '3414 Red Bluff' }])
insurances = Insurance.create([{ name: 'Jimmy Brownies' }, { street_address: '2312 Copenhagen Drive' }])
insurances = Insurance.create([{ name: 'Homalgo' }, { street_address: '3523 Saddle Brook' }])

patients = Patient.create([{ name: 'Action Bronson' }, { street_address: '2563 Radd Street' }, { insurance: 'Blue Corp' }])
patients = Patient.create([{ name: 'Kendrick Lamar' }, { street_address: '2364 Google Drive' }, { insurance: 'Breadwinners' }])
patients = Patient.create([{ name: 'Yung Lean' }, { street_address: '1277 Record Lane' }, { insurance: 'Core Score' }])
patients = Patient.create([{ name: 'Meechy Darko' }, { street_address: '3414 Blue Bluff' }, { insurance: 'Jimmy Brownies' }])
patients = Patient.create([{ name: 'Mac Miller' }, { street_address: '2312 SQL Query Ln' }, { insurance: 'Jimmy Brownies' }])

specialists = Specialist.create([{ name: 'Erick Arc' }, { speciality: 'Cornology' }])
specialists = Specialist.create([{ name: 'GZA' }, { speciality: 'Ruby Mining' }])
specialists = Specialist.create([{ name: 'RZA' }, { speciality: 'Sega Programming' }])
specialists = Specialist.create([{ name: 'Riff Raff' }, { speciality: 'Riddles' }])
specialists = Specialist.create([{ name: 'Joey Badass' }, { speciality: 'Communism' }])

Appointment.create(specialist: 'Erick Arc', patient: 'Kendrick Lamar', complaint: 'No complaints here', appointment_date: '09-10-2016', fee: 5)
Appointment.create(specialist: 'GZA', patient: 'Kendrick Lamar', complaint: 'No complaints here', appointment_date: '12-10-2016', fee: 5)
Appointment.create(specialist: 'GZA', patient: 'Yung Lean', complaint: 'Feeling Woozy', appointment_date: '08-10-2014', fee: 1)
Appointment.create(specialist: 'RZA', patient: 'Yung Lean', complaint: 'Sickly being', appointment_date: '08-10-2015', fee: 3)
Appointment.create(specialist: 'Joey Badass', patient: 'Meechy Darko', complaint: 'Feeling better', appointment_date: '11-10-2014', fee: 2)
Appointment.create(specialist: 'GZA', patient: 'Meechy Darko', complaint: 'No complaints man', appointment_date: '12-10-2015', fee: 7)
Appointment.create(specialist: 'Erick Arc', patient: 'Mac Miller', complaint: 'No complaints here', appointment_date: '11-10-2015', fee: 5)
Appointment.create(specialist: 'GZA', patient: 'Mac Miller', complaint: 'No complaints here', appointment_date: '11-10-2017', fee: 5)
Appointment.create(specialist: 'GZA', patient: 'Action Bronson', complaint: 'Feeling Woozy', appointment_date: '08-10-2015', fee: 1)
Appointment.create(specialist: 'RZA', patient: 'Action Bronson', complaint: 'Sickly being', appointment_date: '12-22-2014', fee: 3)

